[![build status](https://gitlab.com/killbug/website/badges/master/build.svg)](https://gitlab.com/killbug/website/commits/master)

# killbug.com website
This repository contains the landing page of killbug.com

Technologies used :

  * Sass
  * Gulp
  * Browser-sync
  * Webpack

SCSS architecture is inspired of https://github.com/HugoGiraudel/sass-boilerplate

## How to Start
Clone this repository

`git clone https://...`

Run npm install

`npm install`

Start the gulp default task for developping

`npm start`

Then open the address in browser

`localhost:3000`

## How to Build
To build the application run

`npm run build`

## Source

https://dribbble.com/shots/959424-Freebies-My-safari-browser-psd-template
