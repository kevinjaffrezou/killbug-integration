import password from "./components/password";
import session from "./components/session";
import codemirror from "./components/codemirror";
import htmlmixed from "./components/htmlmixed";
import clipboard from "clipboard";
import rellax from "rellax";

password();
session();


// Code
const codeSample = '<script>\n\twindow.killbugtodaySettings = {\n\t\tapp_id: "1a57a222-c770-4979-ba0c-8b7cae8cdfbb",\n\t};\n</script>\n<script>(function(@){var w=window;var ic=w.Killbug;if(typeof ic==="function"){ic("reattach_activator");ic("update",killbugSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement("script");s.type="text/javascript";s.async=true;s.src="https://widget.intercom.io/widget/leqddge1";var x=d.getElementsByTagName("script")[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent("onload",l);}else{w.addEventListener("load",l,false);}}})()</script>';

var myCodeMirror = codemirror($('.js-codemirror-tag').get(0), {
  value: codeSample,
  mode:  "htmlmixed",
	lineNumbers: 1
});

let copypaste = new clipboard('.js-button-copy-codemirror');
$('.js-button-copy-codemirror').attr("data-clipboard-text", codeSample);

copypaste.on('success', function(e) {
  $('.js-button-copy-codemirror').html('<i class="c-icon material-icons">check</i>');
});
